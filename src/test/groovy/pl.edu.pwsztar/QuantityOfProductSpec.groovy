package pl.edu.pwsztar

import spock.lang.Specification

class QuantityOfProductSpec extends Specification {

    static ShoppingCart cart;

    def setupSpec() {
        cart = new ShoppingCart();
    }

    def "should get quantity of products from shopping cart"() {

        when:
        cart.addProducts("p1", 10, 50)

        then:
        cart.getQuantityOfProduct("p1") == 50
        cart.deleteProducts("p1", 1)
        cart.getQuantityOfProduct("p1") == 49
        cart.addProducts("p1", 10, 10)
        cart.getQuantityOfProduct("p1") == 59
        cart.addProducts("p1", 10, 100)
        cart.deleteProducts("p1", 100)
        cart.getQuantityOfProduct("p1") == 59
        cart.deleteProducts("p1", 59)
        cart.getQuantityOfProduct("p1") == 0

    }

}