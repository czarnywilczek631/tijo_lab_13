package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ShoppingCart implements ShoppingCartOperation {

    List<ShoppingCartProduct> products;

    public ShoppingCart() {
        products = new ArrayList<>();
    }

    public boolean addProducts(String productName, int price, int quantity) {
        if (price < 0 || quantity <= 0)
            return false;

        ShoppingCartProduct product = findProduct(productName);

        int newQuantity = getSumProductQuantity() + quantity;
        if (newQuantity > PRODUCTS_LIMIT)
            return false;

        if (product != null) {
            if (product.getPrice() != price)
                return false;

            product.setQuantity(product.getQuantity() + quantity);
        }
        else {
            products.add(new ShoppingCartProduct(productName, price, quantity));
        }
        return true;
    }

    public boolean deleteProducts(String productName, int quantity) {
        if (quantity <= 0)
            return false;

        ShoppingCartProduct product = findProduct(productName);

        if (product != null) {
            if (quantity > product.getQuantity())
                return false;

            int newQuantity = product.getQuantity() - quantity;
            if (newQuantity == 0)
                products.remove(product);
            else
                product.setQuantity(newQuantity);
            return true;
        }
        else return false;
    }

    public int getQuantityOfProduct(String productName) {
        ShoppingCartProduct product = findProduct(productName);
        if (product != null) {
            return product.getQuantity();
        }
        else return 0;
    }

    public int getSumProductsPrices() {
        int sum = 0;
        for(ShoppingCartProduct product : products) {
            sum += product.getQuantity() * product.getPrice();
        }
        return sum;
    }

    public int getProductPrice(String productName) {
        ShoppingCartProduct product = findProduct(productName);
        if (product != null) {
            return product.getPrice();
        }
        else return 0;
    }

    public List<String> getProductsNames() {
        return products.stream().map(ShoppingCartProduct::getProductName).collect(Collectors.toList());
    }

    private ShoppingCartProduct findProduct(String productName) {
        for (ShoppingCartProduct product : products) {
            if (product.getProductName().equals(productName)) {
                return product;
            }
        }
        return null;
    }
    private int getSumProductQuantity() {
        int sum = 0;
        for (ShoppingCartProduct product : products) {
            sum += product.getQuantity();
        }
        return sum;
    }
}
